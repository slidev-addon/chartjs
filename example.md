
<barchart
  json="./data/data.json" 
  :options="{ scales: {y: {display: true, title: {  display: true, text: 'price'}}}, plugins: { title: { display: true , text: 'BarChart powered by chartjs' }}, responsive: true, maintainAspectRatio: false}"
/>